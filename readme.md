Tämä respositoria pitää sisällään HaSeKaan liittyvää dokumentaatiota.

HaSeKa on Suomen Lajitietokeskuksen havainto-, seuranta- ja karoitusjärjestelmä, joka tulee osaksi Laji.fi -sivustoa ja jakaa saman koodipohjan Kokoelmanhallinta Kotkan kanssa.
